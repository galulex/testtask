# Simple User Management app

To setup local environment

Run `bundle install`

Configure your `database.yml`

Setup your local database:

`rails db:setup`

Seed Users with `rails db:seed`
