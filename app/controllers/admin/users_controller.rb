# frozen_string_literal: true

module Admin
  class UsersController < AdminController
    def update
      user.assign_attributes(user_params)
      if user.save
        flash[:notice] = t('.notice')
        redirect_to action: :show, params: {id: user.id}
      else
        render :edit
      end
    end

    def destroy
      User.destroy(params[:id])
      redirect_to action: :index, params: {page: current_page, filters: filter_params}
    end

    private

    delegate :per_page, to: Users
    helper_method :per_page

    def user
      @user ||= User.find(params[:id])
    end
    helper_method :user

    def users
      @users ||= filtered_users.offset((current_page - 1) * per_page).limit(per_page)
    end
    helper_method :users

    def filtered_users
      @filtered_users ||= User.filter(filter_params.to_h)
    end
    helper_method :filtered_users

    def current_page
      params.fetch(:page, 1).to_i
    end
    helper_method :current_page

    def user_params
      params.require(:user).permit(:username, :email, :role)
    end

    def filter_params
      return {} unless params[:filters]
      params.require(:filters).permit(:by_username, :by_email, by_role: []).tap do |filters|
        filters[:by_role]&.reject!(&:blank?)
      end.reject { |_, v| v.blank? }
    end
    helper_method :filter_params
  end
end
