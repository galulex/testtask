# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  helper_method :user_signed_in?
  helper_method :current_user

  private

  def current_user
    return unless session[:user_id]
    @current_user ||= User.find_by(id: session[:user_id])
  end
  alias user_signed_in? current_user

  def authenticate_user!
    session.delete(:user_id) unless user_signed_in?
  end
end
