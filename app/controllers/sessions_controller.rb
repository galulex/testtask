# frozen_string_literal: true

class SessionsController < ApplicationController
  def new
    @user = User.new
  end

  def create
    if user&.authenticate(user_params[:password])
      session[:user_id] = user.id
      redirect_to root_path
    else
      @user = User.new(email: user_params[:email])
      flash.now.alert = t('.alert')
      render :new
    end
  end

  def destroy
    session.delete(:user_id)
    redirect_to root_path
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end

  def user
    @user ||= User.find_by(email: user_params[:email])
  end
  helper_method :user
end
