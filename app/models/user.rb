# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password

  enum role: {
    user:  0,
    admin: 1
  }.freeze

  EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  validates :username, presence: true, uniqueness: true, length: { in: 3..20 }
  validates :email, presence: true, uniqueness: true, format: { with: EMAIL_REGEX }
  validates :password, confirmation: true
  validates :password, length: { minimum: 6 }, on: :create
  validates :role, presence: true

  scope :filter, (lambda do |filters|
    return all if filters.blank?
    filters.map { |filter, value| public_send(filter, value) }.reduce(:merge)
  end)

  scope :by_username, ->(value) { where('username ilike ?', "%#{value}%") }
  scope :by_email, ->(value) { where('email ilike ?', "%#{value}%") }
  scope :by_role, ->(roles) { where(role: roles) }
end
