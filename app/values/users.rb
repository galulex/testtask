module Users
  PER_PAGE = 10
  private_constant :PER_PAGE

  def self.per_page
    PER_PAGE
  end
end
