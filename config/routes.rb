# frozen_string_literal: true

Rails.application.routes.draw do
  resources :registrations, only: %i[create]
  resources :sessions, only: %i[create]

  delete :logout, to: 'sessions#destroy'
  get :login, to: 'sessions#new'
  get :signup, to: 'registrations#new'

  namespace :admin do
    resources :users, except: :create
  end

  root to: 'welcome#index', constraints: ->(request) { request.session[:user_id].present? }
  root to: 'sessions#new'
end
