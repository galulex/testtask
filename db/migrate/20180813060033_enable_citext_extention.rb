# frozen_string_literal: true

class EnableCitextExtention < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'citext'
  end
end
