# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username, null: false, index: true
      t.string :email, null: false, index: true
      t.string :password_digest
      t.integer :role, null: false, index: true, default: 0

      t.timestamps
    end
  end
end
