# frozen_string_literal: true

if Rails.env.development?
  require 'factory_bot_rails'

  FactoryBot.create(:admin, email: 'admin@example.com')
  FactoryBot.create_list(:user, 50)
end
