# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RegistrationsController, type: :controller do
  describe 'GET #new' do
    it 'returns http success' do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #create' do
    let(:params) do
      {
        user: {
          email: 'user@example.com',
          password: 'password',
          password_confirmation: 'password'
        }
      }
    end

    it 'returns http success' do
      get :create, params: params
      expect(response).to have_http_status(:success)
    end
  end
end
