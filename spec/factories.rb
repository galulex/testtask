# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  sequence(:email)          { Faker::Internet.email }
  sequence(:username)       { Faker::Internet.username }
  sequence(:password)       { '1pas$Word' }

  factory :user do
    role :user
    username
    email
    password

    factory :admin do
      role :admin
    end
  end
end
