# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'admin manage users' do
  let(:admin) { create :admin }
  let!(:user) { create :user, username: 'cooldude' }
  let!(:another_user) { create :user }
  let(:valid_user_attributes) { attributes_for(:admin).except(:password).merge(role: 'admin') }

  before do
    sign_in admin
    click_on 'Manage users'
  end

  scenario 'filter, update and delete user' do
    assert_all_users_shown
    filter_users
    assert_users_filtered
    reset_filters
    assert_all_users_shown
    show_user
    update_user_with(valid_user_attributes.merge(username: 'un'))
    assert_user_did_not_update
    update_user_with(valid_user_attributes)
    assert_user_updated
    remove_user
    assert_user_removed
  end

  def show_user
    within "#user_#{user.id}" do
      click_on 'Show'
    end
    expect(page).to have_link('Back')
    click_on 'Edit'
  end

  def update_user_with(attributes)
    within 'form.edit_user' do
      attributes.except(:role).each do |name, value|
        fill_in "user_#{name}", with: value
      end
      choose "user_role_#{attributes[:role]}"
      click_on 'Save'
    end
  end

  def assert_user_did_not_update
    expect(page).to have_content('Username is too short')
  end

  def assert_user_updated
    expect(page).to have_content('User successfully updated')
    expect { user.reload }.to change(user, :email)
    expect(user.reload.attributes.symbolize_keys).to include(**valid_user_attributes)
  end

  def remove_user
    click_on 'Back'
    within "#user_#{user.id}" do
      click_on 'Delete'
    end
  end

  def assert_user_removed
    expect(page).not_to have_content(user.email)
    expect { user.reload }.to raise_error(ActiveRecord::RecordNotFound)
  end

  def filter_users
    within 'form#filters_form' do
      fill_in 'filters_by_username', with: user.username
      fill_in 'filters_by_email', with: user.email
      check 'filters_by_role_user'

      click_on 'Apply'
    end
  end

  def reset_filters
    click_on 'Reset'
  end

  def assert_all_users_shown
    expect(page).to have_css("#user_#{user.id}")
    expect(page).to have_css("#user_#{another_user.id}")
  end

  def assert_users_filtered
    expect(page).to have_css("#user_#{user.id}")
    expect(page).not_to have_css("#user_#{another_user.id}")
  end
end
