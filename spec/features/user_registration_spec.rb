# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'user sign up', type: :feature do
  let(:user) { create(:profile).user }
  let(:valid_attributes) do
    {
      username: 'username',
      email: 'user@example.com',
      password: 'password',
      password_confirmation: 'password'
    }
  end

  before do
    visit root_path
    click_on 'Sign Up'
  end

  scenario do
    sign_up_with(valid_attributes.merge(email: 'invalid'))
    assert_email_invalid
    sign_up_with(valid_attributes.merge(password: 'short'))
    assert_password_invalid
    sign_up_with(valid_attributes.merge(password_confirmation: 'invalid'))
    assert_password_does_not_match
    sign_up_with(valid_attributes)
    assert_user_registered
  end

  def sign_up_with(attributes)
    within 'form#new_user' do
      attributes.each do |attribute_name, value|
        fill_in "user_#{attribute_name}", with: value
      end
      click_on 'Sign Up'
    end
  end

  def assert_user_not_registered
    expect(User.count).to eq(0)
  end

  def assert_user_registered
    assert_logged_in
    expect(User.where(valid_attributes.slice(:username, :email))).to exist
    expect(page).to have_content("Hello #{valid_attributes[:username]}!")
  end

  def assert_email_invalid
    expect(page).to have_content('Email is invalid')
    assert_user_not_registered
  end

  def assert_password_invalid
    expect(page).to have_content('Password is too short')
    assert_user_not_registered
  end

  def assert_password_does_not_match
    expect(page).to have_content("Password confirmation doesn't match")
    assert_user_not_registered
  end
end
