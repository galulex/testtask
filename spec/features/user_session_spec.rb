# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User session' do
  let(:user) { create :user }

  scenario 'signs in and logs out' do
    attempt_sign_in(user, password: 'invalid')
    assert_not_logged_in
    attempt_sign_in(user)
    assert_logged_in
    logout
    assert_logged_out
  end

  def assert_not_logged_in
    expect(page).to have_content('Invalid email or password')
  end

  def logout
    click_on 'Logout'
  end

  def assert_logged_out
    expect(page).to have_link('Log In')
  end
end
