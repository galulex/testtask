# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#valid?' do
    let!(:existing_user) { create :user }

    it { is_expected.to define_enum_for(:role) }
    it { is_expected.to validate_presence_of(:role) }
    it { is_expected.to validate_presence_of(:username) }
    it { is_expected.to validate_uniqueness_of(:username) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_uniqueness_of(:email) }
    it { is_expected.to validate_confirmation_of(:password) }
    it { is_expected.not_to allow_value('incalid@email').for(:email) }
  end

  describe 'scopes' do
    let!(:first_user) { create :user }
    let!(:second_user) { create :admin }

    describe '.by_username' do
      subject { described_class.by_username(first_user.username) }

      it { is_expected.to include(first_user) }
      it { is_expected.not_to include(second_user) }
    end

    describe '.by_email' do
      subject { described_class.by_email(first_user.email) }

      it { is_expected.to include(first_user) }
      it { is_expected.not_to include(second_user) }
    end

    describe '.by_role' do
      subject { described_class.by_role(first_user.role) }

      it { is_expected.to include(first_user) }
      it { is_expected.not_to include(second_user) }
    end
  end
end
