# frozen_string_literal: true

module SessionFeatureTestHelper
  def sign_in(user)
    attempt_sign_in(user)
    assert_logged_in
  end

  def attempt_sign_in(user, password: generate(:password))
    visit login_path
    within 'form#sign_in_form' do
      fill_in 'user_email', with: user.email
      fill_in 'user_password', with: password
      first('input[type="submit"]').click
    end
  end

  def assert_logged_in
    expect(page).to have_link('Logout')
  end
end

RSpec.configure do |config|
  config.include SessionFeatureTestHelper, type: :feature
end
